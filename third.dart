//creating the class App
class App {
  String app_name;
  String sector;
  String developer;
  int year_won;

  App(this.app_name, this.developer, this.sector, this.year_won) {}

//a method to print all the information of the class object
  void describe() {
    print(
        'App name : $app_name \nSector: $sector \nDeveloper: $developer \nYear: $year_won');
  }

//A function which converts the name of the app to uppercase
  void toCapital() {
    print(app_name.toUpperCase());
  }
}

void main() {
  App my_app =
      new App('Shyft', 'Standard Bank', 'Banking', 2017); // class instance

  my_app.describe(); //first method which prints the app_name, developer, sector and the year it won the MTN App Award
  my_app.toCapital();
}
